﻿namespace WebApi.Models.Devices
{
    public class UserDevices
    {
        public int UserId { get; set; }

        public int[] DevicesIds { get; set; }
    }
}
