﻿namespace WebApi.Models.Devices
{
    public class CreateDeviceRequest
    {
        public string RemoteControlTitle { get; set; }

        public string RemoteControlPath { get; set; }
    }
}
