namespace WebApi.Models;

public class RemoteControlPathResponse
{
    public int Id { get; set; }

    public string RemoteControlTitle { get; set; }

    public string RemoteControlPath { get; set; }
}