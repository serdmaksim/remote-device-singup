﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Models;
using WebApi.Models.Devices;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class DevicesController : BaseController
    {
        private readonly IAccountService _accountService;
        private readonly IDeviceService deviceService;

        public DevicesController(
            IAccountService accountService,
            IDeviceService deviceService)
        {
            _accountService = accountService;
            this.deviceService = deviceService;
        }

        [HttpGet("{id:int}")]
        public Task<RemoteControlPathResponse[]> GetByUserId(int id)
        {
            Task<RemoteControlPathResponse[]> response = deviceService.GetByUserId(id);
            return response;
        }

        [HttpGet()]
        [Route("getAll")]
        public Task<RemoteControlPathResponse[]> GetAllDevices()
        {
            // users can get their own account and admins can get any account
            if (Account.Role != Role.Admin)
            {
                return Task.FromResult<RemoteControlPathResponse[]>(null);
            }

            Task<RemoteControlPathResponse[]> response = deviceService.GetAll();
            return response;
        }

        [Authorize(Role.Admin)]
        [HttpPost]
        public async Task<ActionResult<RemoteControlPathResponse>> Create(CreateDeviceRequest model)
        {
            RemoteControlPathResponse device = await deviceService.Create(model, Account.Id);
            return Ok(device);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            // users can delete their own account and admins can delete any account
            if (id != Account.Id && Account.Role != Role.Admin)
            {
                return Unauthorized(new { message = "Unauthorized" });
            }

            await deviceService.Delete(id);
            return Ok(new { message = "Device deleted successfully" });
        }

        //[Authorize(Role.Admin)]
        [AllowAnonymous]
        [HttpPut]
        [Route("UpdateUserDevices")]
        public async Task<ActionResult<UserDevices>> UpdateUserListOfDevices(
            UserDevices model)
        {
            UserDevices devices = await deviceService.ApplyListOfDevices(model);
            return Ok(devices);
        }
    }
}
