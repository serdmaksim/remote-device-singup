﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models;
using WebApi.Models.Devices;

namespace WebApi.Services
{
    public interface IDeviceService
    {
        Task<RemoteControlPathResponse[]> GetByUserId(int id);

        Task<RemoteControlPathResponse[]> GetAll();

        Task<RemoteControlPathResponse> Create(CreateDeviceRequest model, int userId);

        Task<int> Delete(int id);

        Task<UserDevices> ApplyListOfDevices(UserDevices model);
    }

    public class DeviceService : IDeviceService
    {
        private readonly DataContext _context;
        private readonly IMapper mapper;

        public DeviceService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            this.mapper = mapper;
        }

        public async Task<RemoteControlPathResponse[]> GetByUserId(int id)
        {
            List<DevicesPath> remoteControlPaths =
                await _context.Devices
                .Include(x => x.Accounts)
                .Where(x => x.Accounts.Any(a => a.Id == id))
                .ToListAsync();
            RemoteControlPathResponse[] response =
                mapper.Map<RemoteControlPathResponse[]>(remoteControlPaths);
            return response;
        }

        public async Task<RemoteControlPathResponse[]> GetAll()
        {
            List<DevicesPath> remoteControlPaths =
                await _context.Devices
                .Include(x => x.Accounts)
                .ToListAsync();
            RemoteControlPathResponse[] response =
                mapper.Map<RemoteControlPathResponse[]>(remoteControlPaths);
            return response;
        }

        public async Task<RemoteControlPathResponse> Create(CreateDeviceRequest model, int userId)
        {
            if (_context.Devices.Any(
                x => x.RemoteControlPath == model.RemoteControlPath ||
                x.RemoteControlTitle == model.RemoteControlTitle))
            {
                throw new AppException($"Device '{model.RemoteControlPath}' is already registered");
            }

            DevicesPath device = mapper.Map<DevicesPath>(model);
            Account account = _context.Accounts.FirstOrDefault(a => a.Id == userId);
            device.Accounts = new List<Account> { account };
            _context.Devices.Add(device);
            await _context.SaveChangesAsync();

            return mapper.Map<RemoteControlPathResponse>(device);
        }

        public Task<int> Delete(int id)
        {
            DevicesPath device =
                 _context.Devices
                .Include(x => x.Accounts)
                .FirstOrDefault(x => x.Id == id);
            if (device == null)
            {
                throw new KeyNotFoundException("Device not found");
            }
            _context.Devices.Remove(device);
            _context.SaveChanges();

            return Task.FromResult(id);
        }

        public async Task<UserDevices> ApplyListOfDevices(UserDevices model)
        {
            List<DevicesPath> devices =
                _context.Devices
                    .Where(d => model.DevicesIds.Contains(d.Id))
                    .ToList();
            Account account =
                _context.Accounts
                    .Include(a => a.Devices)
                    .FirstOrDefault(a => a.Id == model.UserId);
            account.Devices = devices;
            _context.Accounts.Update(account);
            await _context.SaveChangesAsync();

            return model;
        }
    }
}
