﻿namespace WebApi.Entities
{
    public class DevicesPath
    {
        public int Id { get; set; }

        public string RemoteControlTitle { get; set; }

        public string RemoteControlPath { get; set; }

        public List<Account> Accounts { get; set; }
    }
}
